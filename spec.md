# Draft Specification #9
This is a draft for the titular network protocol.

## Rationale
'Do one thing, and do it well.' - This quote applies to every piece of software. This aims to do the topic of deciding where data goes next. It does not touch the concept of general network routing, rather the concept of routing that is required to happen on every node, from terminals to core routing nodes.

## Target Networks
Here no assumptions about the network in use are made. It could be a network 'stack' made by Izaya, Skye, Magi6k or even CompanionCube. However, in order to ensure that everything can work correctly, I will assume the following constraints are true in all use cases.
1. The network has at least 2 nodes. It is recommended to use more however, due to point-to-point communication being another method with only 2 nodes.
2. All nodes on the network can communicate in some form. Intermediate nodes may be required, but nodes cannot not communicate while on the network.
3. Individual nodes are identifiable. Routing cannot happen unless Node A can be distinguished between Node B. Uniqueness of node identification is not strictly required but is recommended.
4. All routes are available at runtime. All connections are presumed to be persistent. All connections are assumed to work unless it is known when and if they won't / can't.
5. All routes are static. Dynamic routing can be achieved by other protocols / programs modifying the list of static routes.

## System Initialization
When ever the system boots (or when the network stack is initalized, whichever is prefered) then the routing component should also initialize. Initalization is the loading of configuration and creation of a local routing table. The local routing table is created based solely on the configuration but may later be altered via an API. Configuration consists of a default gateway plus an optional list of static routes to be added into the routing table. Both of these should be stored on persistent media or otherwise retained between system uptimes.

### Modes of Operation
2 main modes of operation would fit very well: A standalone routing system daemon, and incorporation of the core functionality as a component of another program. Both should provide interfaces to modify the routing table.

## System Operation
After the initialization procedures described in the above heading, the component is able to serve routing requests from any and all local clients. The API as described in this document does not bind to any network interface, and this functionality should be implement by another software component. Addditionally, how the API is interacted with by clients is not defined here - only the available commands/operations and their results. All runtime modifications of the routing table should take place via the API. Static configuration information may or may not take precedence over information provided during runtime.

## API
As mentioned above, the routing component exposes an API to other programs running on the same box. This API is simple, and allows the requesting of routes and modification of the local routing table.

API Operation | Description                                                                                                        | Parameters                                                 | Response
------------- | ------------------------------------------------------------------------------------------------------------------ | ---------------------------------------------------------- | ---------------------------------------------------------
VERSION       | Obtain the version                                                                                                 |                                                            | The version number of the component.
ROUTES        | Obtain the current list of routes in the local routing table                                                       |                                                            | A dump of the local routing table in the standard format.
GET_ROUTE     | Obtain a route to the specified location from the specified source. Optionally does not exceed the specified cost. | source_address, destination, max_cost (optional)           | The destination address that the selected route leads to.
ADD_ROUTE     | Add the specified route to the local routing table.                                                                | source_address, destination_address, route_address, metric | 'OK' or 'FAIL'
REMOVE_ROUTE  | Remove the specified route from the tablet.                                                                        | Same as above                                              | 'OK' or 'FAIL'

## Configuration
As mentioned in System Initialization, the component initalizes itself when started. Configuration comes from local files stored under /etc or an equivalent location. There are 2 different segments of configuration: the Default Gateway and a initial Routing Table. The Default Gateway should be an address that can be reached without external routing - i.e a directly connected one. If the network architecture is not conducive to the usage of a Default Gateway then it should be left unset or set to a known bogus value. The Routing Table should follow a single standard format like the one below. ``` %%NaRIS-Routing-Table

<source-address>;<destination-address>;<target-address>;<metric> (Optional) One line per initial route entry. Optionally, the runtime table may be flushed to disk when the component shuts down.
